require 'set'

class Node
  attr_reader :value, :edges

  def initialize(value)
    @value = value
    @edges = Set.new
  end

  def add_edge(node)
    @edges.add node
  end
end