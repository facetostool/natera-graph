class PathFinder
  def self.find_path_between(graph, value1, value2)
    PathFinder.new(graph).find_path_between(value1, value2)
  end

  def initialize(graph)
    @graph = graph
    @visited = {}
  end

  def find_path_between(value1, value2)
    node1 = ensure_node(value1)
    return [value1] if value1 == value2
    node2 = ensure_node(value2)
    path = path_between(node1, node2, [node1.value])
    raise NoPathError.new(value1, value2) unless path
    path
  end

  private

  def path_between(node1, node_to_find, path)
    return path if node1 == node_to_find
    node1.edges.each do |node|
      next if @visited[node]
      @visited[node] = true
      path << node.value
      return path_between(node, node_to_find, path)
    end
    nil
  end

  def ensure_node(value)
    @graph.node(value) || raise(NodeNotFoundError.new(value))
  end

  class PathFinderError < StandardError; end

  class NodeNotFoundError < PathFinderError
    def initialize(value)
      super("No node with #{value} in graph")
    end
  end

  class NoPathError < PathFinderError
    def initialize(value1, value2)
      super("No path between #{value1} #{value2} in graph")
    end
  end
end