require_relative 'node'

class Graph
  attr_reader :nodes

  def node(value)
    @nodes[value]
  end

  def initialize
    @nodes = {}
  end

  def add_node(value)
    @nodes[value] ||= Node.new(value)
  end

  def exist?(value)
    @nodes[value] != nil
  end

  def add_edge(value1, value2)
    node1 = add_node(value1)
    node2 = add_node(value2)
    node1.add_edge(node2)
  end
end