FROM ruby:2.6.4-alpine

ADD . /usr/local/natera-graph

WORKDIR /usr/local/natera-graph

RUN gem install bundle && bundle