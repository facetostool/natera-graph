require 'spec_helper'

describe Graph do
  let(:subject) { Graph.new }
  let(:node_value) { 1 }

  describe '#add_node' do
    it 'adds node to graph' do
      node = subject.add_node(node_value)
      expect(node).to be_a Node
      expect(node.value).to eq(node_value)
    end

    context 'when already added' do
      it 'returns the same' do
        node1 = subject.add_node(node_value)
        node2 = subject.add_node(node_value)
        expect(node1).to eq(node2)
      end
    end
  end

  describe '#exist?' do
    context 'when node was added' do
      before { subject.add_node(node_value) }

      it 'true' do
        expect(subject.exist?(node_value)).to be_truthy
      end
    end

    context 'when no nodes' do
      it 'false' do
        expect(subject.exist?(node_value)).to be_falsey
      end
    end
  end

  describe '#add_node' do
    context 'when need to create an edge in single direction' do
      it 'creates right edges' do
        subject.add_edge(1, 2)
        expect(subject.nodes.size).to eq 2
        node1 = subject.node(1)
        node2 = subject.node(2)
        expect(node1.edges.size).to eq 1
        expect(node1.edges.first).to eq node2
        expect(node2.edges).to be_empty
      end
    end

    context 'when need to create an edge in both directions' do
      it 'creates right edges' do
        subject.add_edge(1, 2)
        subject.add_edge(2, 1)
        expect(subject.nodes.size).to eq 2
        node1 = subject.node(1)
        node2 = subject.node(2)
        expect(node1.edges.size).to eq 1
        expect(node1.edges.first).to eq node2
        expect(node2.edges.size).to eq 1
        expect(node2.edges.first).to eq node1
      end
    end
  end
end