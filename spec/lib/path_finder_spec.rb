require 'spec_helper'

describe PathFinder do
  subject { PathFinder.new(graph) }
  let(:graph) do
    graph = Graph.new
    graph.add_edge(1,2)
    graph.add_edge(2,3)
    graph.add_edge(3,4)
    graph
  end

  describe '#find_path_between' do
    context 'when path exists' do
      it 'returns right path' do
        expect(subject.find_path_between(2, 4)).to eq([2,3,4])
      end
    end

    context 'when path does not exist' do
      it 'raises error' do
        expect{
          subject.find_path_between(3, 1)
        }.to raise_error PathFinder::NoPathError
      end
    end

    context 'when node does not exist' do
      it 'raises error' do
        expect{
          subject.find_path_between(3, 10)
        }.to raise_error PathFinder::NodeNotFoundError
      end
    end
  end
end