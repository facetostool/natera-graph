require 'spec_helper'

describe Node do
  let(:value) { 1 }
  subject { Node.new(value) }

  describe '#add_edge' do
    it 'creates an edge' do
      node2 = Node.new(2)
      subject.add_edge(node2)
      expect(subject.edges.first).to eq(node2)
    end

    context 'when need to create edge for the same node' do
      it 'does not create duplicates' do
        node2 = Node.new(2)
        subject.add_edge(node2)
        subject.add_edge(node2)
        expect(subject.edges.size).to eq(1)
      end
    end
  end
end